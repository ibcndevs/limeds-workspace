package example.helloworld;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.ApplySchedule;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.json.JsonValue;

/**
 * This example LimeDS segment is scheduled to say hello when starting the
 * framework!
 * 
 * @author wkerckho
 *
 */
@ApplySchedule("delay 0 seconds")
@Segment
public class Hello implements FunctionalSegment {

	@Configurable(description = "The message to print", defaultValue = "Hello from LimeDS instance!")
	private String message;

	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		System.out.println(message);
		return null;
	}

}
